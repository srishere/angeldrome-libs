<h1>App Reported An Error!!</h1>
<div class="content">
    <p>Error: {%error_number%}::{%error_message%}</p>
    <p>File: {%file%}</p>
    <p>Line Number: {%line%}</p>
    <p>
            Data: <br />
            <pre>
                {%data%}
            </pre>
    </p>
    <p>Session ID: {%sess_id%}</p>
    <div class="content-footer">This email blah blah AngeldromeLibs blah blah... You can customize to meet your organization need.</div>
</div>
