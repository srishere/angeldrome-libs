<div class="content">
    <div class="content-title">{%title%}</div>
    <div class="content-body">{%content%}</div>
    <div class="content-footer">This email blah blah AngeldromeLibs blah blah... You can customize to meet your organization need.</div>
</div>
