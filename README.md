[![Some core libs that can be used for app development.](https://www.angeldrome.com/img/logo.png)](https://www.angeldrome.com/)
# Angeldrome Libs

A very minimal queuing system for asynchronous email sending and db write. Can be extended for logger among other similar IO and response delay operations.

  This is very much in incubation as now. Am using SQLite for db as now and pretty much can be turned into anything that suits.

  Code works, tested email sending and db writes and logger store. All went good.

  Cron dependencies are there. such as
  1. Please check src/Com/Angeldrome/Shells/Cron.php on how to use for email async.
  2. DBResume.php can be used via cron as well (code is in src/Com/Angeldrome/Shells/ directory).
     This is used in case the db write failed in an earlier instancelike timeout or anything.
     Lib writes such records in a flat text file, and this Cron Shell can get the record and push to db.

Will improve this readme at a later time.
