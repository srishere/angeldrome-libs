<?php
/*
 * AngeldromeLibs, Some Base classes that could be reused for app development.
 *
 * the error page that will be rendered, when errors are caught.
 *
 * @author     Sriram R <srishere@angeldrome.com>
 * @copyright  2020 Sriram R
 * @license    This code is licensed under MIT license (see LICENSE.txt for details)
 * @version    CVS: $Id:$
 * @link       http://www.angeldrome.com
 */
?>
<html>
    <head>
        <title></title>
    </head>
    <body>
        lets have a fancy error page.
    </body>
</html>
