<?php

namespace spec\Com\Angeldrome\Configs;

use Com\Angeldrome\Configs\Messages;
use PhpSpec\ObjectBehavior;

class MessagesSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(Messages::class);
    }
}
