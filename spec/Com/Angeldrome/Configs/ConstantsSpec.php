<?php

namespace spec\Com\Angeldrome\Configs;

use Com\Angeldrome\Configs\Constants;
use PhpSpec\ObjectBehavior;

class ConstantsSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(Constants::class);
    }
}
