<?php

namespace spec\Com\Angeldrome\Configs;

use Com\Angeldrome\Configs\SQLs;
use PhpSpec\ObjectBehavior;

class SQLsSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(SQLs::class);
    }
}
