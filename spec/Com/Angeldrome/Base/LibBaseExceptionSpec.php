<?php

namespace spec\Com\Angeldrome\Base;

use Com\Angeldrome\Base\LibBaseException;
use PhpSpec\ObjectBehavior;

class LibBaseExceptionSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(LibBaseException::class);
    }
}
