<?php

namespace spec\Com\Angeldrome\Base;

use Com\Angeldrome\Base\LibBase;
use PhpSpec\ObjectBehavior;

class LibBaseSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(LibBase::class);
    }
}
