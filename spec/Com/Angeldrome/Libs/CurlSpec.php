<?php

namespace spec\Com\Angeldrome\Libs;

use Com\Angeldrome\Libs\Curl;
use PhpSpec\ObjectBehavior;

class CurlSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(Curl::class);
    }
}
