<?php

namespace spec\Com\Angeldrome\Libs;

use Com\Angeldrome\Libs\SQLite3;
use PhpSpec\ObjectBehavior;

class SQLite3Spec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(SQLite3::class);
    }
}
