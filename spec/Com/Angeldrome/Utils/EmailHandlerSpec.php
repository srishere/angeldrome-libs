<?php

namespace spec\Com\Angeldrome\Utils;

use Com\Angeldrome\Utils\EmailHandler;
use PhpSpec\ObjectBehavior;

class EmailHandlerSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(EmailHandler::class);
    }
}
