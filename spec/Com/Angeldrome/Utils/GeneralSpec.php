<?php

namespace spec\Com\Angeldrome\Utils;

use Com\Angeldrome\Utils\General;
use PhpSpec\ObjectBehavior;

class GeneralSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(General::class);
    }
}
