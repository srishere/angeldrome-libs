<?php

namespace spec\Com\Angeldrome\Utils;

use Com\Angeldrome\Utils\Logger;
use PhpSpec\ObjectBehavior;

class LoggerSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(Logger::class);
    }
}
