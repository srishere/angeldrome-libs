<?php

namespace spec\Com\Angeldrome\Exceptions;

use Com\Angeldrome\Exceptions\DBException;
use PhpSpec\ObjectBehavior;

class DBExceptionSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(DBException::class);
    }
}
