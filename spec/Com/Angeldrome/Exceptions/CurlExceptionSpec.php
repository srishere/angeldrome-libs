<?php

namespace spec\Com\Angeldrome\Exceptions;

use Com\Angeldrome\Exceptions\CurlException;
use PhpSpec\ObjectBehavior;

class CurlExceptionSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(CurlException::class);
    }
}
