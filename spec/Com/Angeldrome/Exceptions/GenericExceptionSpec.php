<?php

namespace spec\Com\Angeldrome\Exceptions;

use Com\Angeldrome\Exceptions\GenericException;
use PhpSpec\ObjectBehavior;

class GenericExceptionSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(GenericException::class);
    }
}
