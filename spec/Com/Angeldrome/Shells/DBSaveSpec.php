<?php

namespace spec\Com\Angeldrome\Shells;

use Com\Angeldrome\Shells\DBSave;
use PhpSpec\ObjectBehavior;

class DBSaveSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(DBSave::class);
    }
}
