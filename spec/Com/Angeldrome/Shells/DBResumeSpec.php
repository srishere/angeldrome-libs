<?php

namespace spec\Com\Angeldrome\Shells;

use Com\Angeldrome\Shells\DBResume;
use PhpSpec\ObjectBehavior;

class DBResumeSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(DBResume::class);
    }
}
