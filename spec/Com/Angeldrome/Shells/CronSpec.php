<?php

namespace spec\Com\Angeldrome\Shells;

use Com\Angeldrome\Shells\Cron;
use PhpSpec\ObjectBehavior;

class CronSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(Cron::class);
    }
}
