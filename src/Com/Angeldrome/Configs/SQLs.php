<?php
/*
 * AngeldromeLibs, Some Base classes that could be reused for app development.
 *
 * SQLs class for the fixed SQLs that are used in various classes.
 *
 * @package    Com\Angeldrome\Utils
 * @author     Sriram R <srishere@angeldrome.com>
 * @copyright  2020 Sriram R
 * @license    This code is licensed under MIT license (see LICENSE.txt for details)
 * @version    CVS: $Id:$
 * @link       http://www.angeldrome.com
 */

    namespace Com\Angeldrome\Configs;

    use Com\Angeldrome\Configs\Constants;

    class SQLs
    {
        public const EMAIL_TABLE = "emails";
        public const LOG_TABLE = "logs";
        public const CHECK_EMAIL_TABLE = "SELECT name FROM sqlite_master WHERE type='table' AND name='" . self::EMAIL_TABLE . "'";
        public const CHECK_LOGGER_TABLE = "SELECT name FROM sqlite_master WHERE type='table' AND name='" . self::LOG_TABLE . "'";
        public const SELECT_EMAIL_DATA = "SELECT id, message, attempts from " . self::EMAIL_TABLE . " WHERE delivered = 0 AND aborted = 0";
        public const CREATE_EMAIL_TABLE = "CREATE TABLE " . self::EMAIL_TABLE . " ( id INTEGER PRIMARY KEY, message TEXT, delivered_ts DATETIME, delivered INT, attempts INT, aborted INT, last_error TEXT, created DATETIME, modified DATETIME)";
        public const CREATE_LOGGER_TABLE = "CREATE TABLE " . self::LOG_TABLE . " ( id INTEGER PRIMARY KEY, asset_id INTEGER NOT NULL, severity VARCHAR(20), is_object INTEGER, message TEXT, ip_address VARCHAR(50), user_agent VARCHAR(512), created DATETIME, modified DATETIME)";
    }
