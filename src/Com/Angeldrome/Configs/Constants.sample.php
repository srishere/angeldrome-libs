<?php
/*
 * AngeldromeLibs, Some Base classes that could be reused for app development.
 *
 * Constants class used for various configurations
 *
 * @package    Com\Angeldrome\Configs
 * @author     Sriram R <srishere@angeldrome.com>
 * @copyright  2020 Sriram
 * @license    This code is licensed under MIT license (see LICENSE.txt for details)
 * @version    0.1
 * @link       http://www.angeldrome.com
 */

    namespace Com\Angeldrome\Configs;

    class Constants
    {
        public const APP_TAG = "Angeldrome";
        public const DS = "/";

        /*
         * If this is set to true, app will try to create databases and other core items.
         * If any of assets found pre-existing, install mode goes aborted.
         * For faster execution, set this to false, after first time.
         *
        */
        public const INSTALL_MODE = true;

        /*
         * If this is set to true, then we get to access the methods as API calls.
         * If internal use, this be false.
        */
        public const API_MODE = true;

        // lets store logs in sqlite, that we can do some nice reporting.
        public const LOGGER_DB = "oup_logs.sqlite3";
        // to store emails in queue, till delivery
        public const EMAIL_DB = "oup_email.sqlite3";
        public const EMAIL_MAX_ATTEMPTS = 5;
        public const EMAIL_DEBUG_ON = 4;

        public const SMTP_HOST = "smtp.gmailllllllll.con";
        public const SMTP_PORT = "58777777777";
        public const SMTP_USER = "noreply@yourorg.con";
        public const SMTP_PASS = "**************";
        public const SMTP_FROM = [ self::SMTP_USER, self::APP_TAG ];


        public const SMTP_TLS_REQUIRED = true;
        public const SMTP_HAS_AUTH = true;
        public const EMAIL_TEMPLATE_DEFAULT = "default";
        public const EMAIL_TEMPLATE_ERRORS = "errors";
        public const EMAIL_DEFAULT_SUBJECT = "Mail From ".self::APP_TAG.".";

        /*
         * all internal information that require to be sent in email to say an admin user...
         * if this is having the default not_used as values, then no email will go...
         * data in form Name, Email
         */
        public const INFO_TO = [ "name" => "NOT_USED", "email" => "NOT_USED"];

        public const LOGGER_SEVERITY_ALL = "ALL";
        public const LOGGER_SEVERITY_INFO = "INFO";
        public const LOGGER_SEVERITY_DEBUG = "DEBUG";
        public const LOGGER_SEVERITY_ERROR = "ERROR";

        public const RUN_EMAIL = "email";

        public const SQLITE3_TIMEOUT_SECONDS = 15;
        public const DB_OPERATION_INSERT = "insert";
        public const DB_OPERATION_UPDATE = "update";

        /*
         *
         * name: getHomePath
         *
         * Method that returns the home path of this app, that it can be prefixed to form absolute path for assets.
         *
         * @return string  the root directory of this package
         * @access public
         * @static
        */

        public static function getHomePath()
        {
            return dirname(__FILE__, 5);
        }

        /*
         *
         * name: getVendorPath
         *
         * Method that returns the vendor path of this app, that it can be prefixed to form absolute path for vendor libraries.
         *
         * @return string  the vendor directory of this package
         * @access public
         * @static
        */

        public static function getVendorPath()
        {
            return self::getHomePath().self::DS."vendor";
        }

        /*
         *
         * name: getDBDirectory
         *
         * return the directory where the database files are kept.
         *
         * @return string  directory path of database files.
         * @access public
         * @static
        */

        public static function getDBDirectory()
        {
            return self::getHomePath().self::DS."private";
        }
    }
