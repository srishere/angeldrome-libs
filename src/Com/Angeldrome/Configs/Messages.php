<?php
/*
 * AngeldromeLibs, Some Base classes that could be reused for app development.
 *
 * Messages class used for lang.
 *
 * @package    Com\Angeldrome\Utils
 * @author     Sriram R <srishere@angeldrome.com>
 * @copyright  2020 Sriram R
 * @license    This code is licensed under MIT license (see LICENSE.txt for details)
 * @version    CVS: $Id:$
 * @link       http://www.angeldrome.com
 */

    namespace Com\Angeldrome\Configs;

    class Messages
    {
        public const DB_INIT_FAIL_MSG = "DB init Failed";
        public const DB_INIT_FAIL_CODE = 25001;

        public const DB_INSERT_FAIL_MSG = "Insert Failed";
        public const DB_INSERT_FAIL_CODE = 25002;

        public const DB_UPDATE_FAIL_MSG = "Update Failed";
        public const DB_UPDATE_FAIL_CODE = 25003;

        public const DB_SELECT_FAIL_MSG = "Select Failed";
        public const DB_SELECT_FAIL_CODE = 25004;

        public const LOGGER_DB_MISSING_MSG = "Logger asset missing. Please setup environment, per instructions.";
        public const LOGGER_DB_MISSING_CODE = 21000;

        public const NO_CURL_MSG = "PHP Curl Not Found";
        public const NO_CURL_CODE = 21001;

        public const NO_SQLITE3_MSG = "SQLITE3 php extension missing. Please setup environment, per instructions.";
        public const NO_SQLITE3_CODE = 21002;

        public const EMAILER_DB_MISSING_MSG = "Email asset missing. Please setup environment, per instructions.";
        public const EMAILER_DB_MISSING_CODE = 21003;

        public const LOGGER_DB_PERM_MSG = "Logger db file non writable. Please setup environment, per instructions.";
        public const LOGGER_DB_PERM_CODE = 21004;

        public const EMAILER_DB_PERM_MSG = "Emailer db file non writable. Please setup environment, per instructions.";
        public const EMAILER_DB_PERM_CODE = 21004;

        public const UNDEFINED_EXCEPTION_MSG = "Undefined Exception";
        public const UNDEFINED_EXCEPTION_CODE = 20000;

        public const NO_SUITABLE_CRON = "No Suitable Cron To Run";
        public const ERROR_EMAIL_SUBJECT = "[ Angeldrome ] Error Notification.";
    }
