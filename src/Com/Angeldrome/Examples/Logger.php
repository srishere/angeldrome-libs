<?php
/*
 * AngeldromeLibs, Some Base classes that could be reused for app development.
 *
 * Example Client Class for creating logs
 *
 * @package    Com\Angeldrome\Examples
 * @author     Sriram R <srishere@angeldrome.com>
 * @copyright  2020 Sriram R
 * @license    This code is licensed under MIT license (see LICENSE.txt for details)
 * @link       http://www.angeldrome.com
 */

namespace Com\Angeldrome\Examples;

use Com\Angeldrome\Base\LibBase;
use Com\Angeldrome\Configs\Constants;

class Logger extends LibBase
{
    /*
     *
     * name: __construct
     *
     * Constructor. Ensure that the Constants Class INSTALL_MODE is set to true.
     *
     * @see Com\Angeldrome\Configs\Constants
     *
     * @throws exceptionclass  [description]
     * @access public
     *
    */

    public function __construct()
    {
        parent::__construct();
    }

    public function runExamples()
    {
        $data = "This is an Info Log Data as a string";
        $this->info($data);
        $data = ["key1" => "value1", "key2" => "value2"];
        $this->debug($data);
        $data = (object) $data;
        $this->error($data);
        $logger = new \Com\Angeldrome\Utils\Logger();
        $this->printLog("All", $logger->getLogs());

        //($dated = null, $severity = Constants::LOGGER_SEVERITY_ALL, $pattern = "")
        //you can go with each of those params to get what you want...
        //if required extend this to work for a range of date etc...
        $this->printLog("Debugs", $logger->getLogs(null, Constants::LOGGER_SEVERITY_DEBUG));

    }

    private function printLog($title, $data)
    {
        print "\n\n".$title." Log.\n\n";
        print_r(json_decode($data));
        print "\n\n";
    }
}
