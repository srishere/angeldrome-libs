<?php
/*
 * AngeldromeLibs, Some Base classes that could be reused for app development.
 *
 * Example Client Class for queuing emails
 *
 * @package    Com\Angeldrome\Examples
 * @author     Sriram R <srishere@angeldrome.com>
 * @copyright  2020 Sriram R
 * @license    This code is licensed under MIT license (see LICENSE.txt for details)
 * @link       http://www.angeldrome.com
 */

namespace Com\Angeldrome\Examples;

use Com\Angeldrome\Base\LibBase;

class Emailer extends LibBase
{
    public function __construct()
    {
        $to_email = "someone@someorg.con";
        $to_name = "Some One";
        $this->sendEmail($to_email, $to_name, ["title" => "Email Title", "content" => "Email Content"], "A TEST EMAIL", Constants::EMAIL_TEMPLATE_DEFAULT);
        \Com\Angeldrome\Shells\Cron::run();
    }
}
