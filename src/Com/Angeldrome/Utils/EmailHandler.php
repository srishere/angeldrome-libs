<?php
/*
 * AngeldromeLibs, Some Base classes that could be reused for app development.
 *
 * EmailHandler Utility class, to send fault tolerant emails asynchronously.
 *
 * @package    Com\Angeldrome\Utils
 * @author     Sriram R <srishere@angeldrome.com>
 * @copyright  2020 Sriram R
 * @license    This code is licensed under MIT license (see LICENSE.txt for details)
 * @version    CVS: $Id:$
 * @link       http://www.angeldrome.com
 */

namespace Com\Angeldrome\Utils;

use Com\Angeldrome\Configs\Constants;

$pmailer_base = Constants::getVendorPath() . Constants::DS . "phpmailer" . Constants::DS . "phpmailer" . Constants::DS . "src" . Constants::DS;
include($pmailer_base . "PHPMailer.php");
include($pmailer_base . "Exception.php");
include($pmailer_base . "SMTP.php");



use Com\Angeldrome\Configs\Messages;
use Com\Angeldrome\Configs\SQLs;
use Com\Angeldrome\Libs\SQLite3;
use Com\Angeldrome\Shells\DBSave;
use Com\Angeldrome\Exceptions\DBException;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;


class EmailHandler
{
    private $mail = null;
    private $db = null;

    /*
     *
     * name: __construct
     *
     * Default Constructor, that does the db connection and basic email settings.
     *
     * @param boolean $first_time to be used via an installer that the schema can be created, if this flag is set as true.
     *
     * @access public
     *
    */

    public function __construct($first_time = false)
    {
        if ($first_time) {
            $this->db = new SQLite3(Constants::EMAIL_DB);
            $this->createSchema();
        }
        if(file_exists(Constants::getDBDirectory().Constants::DS.Constants::EMAIL_DB)) {
            if(is_writeable(Constants::getDBDirectory().Constants::DS.Constants::EMAIL_DB)) {
                $this->db = new SQLite3(Constants::EMAIL_DB);
            } else {
                throw new DBException(Messages::EMAILER_DB_PERM_MSG, Messages::EMAILER_DB_PERM_CODE);
            }
        } else {
            throw new DBException(Messages::EMAILER_DB_MISSING_MSG, Messages::EMAILER_DB_MISSING_CODE);
        }

        $this->mail = new PHPMailer();
        $this->mail->isSMTP();
        $this->mail->SMTPDebug = Constants::EMAIL_DEBUG_ON;
        $this->mail->Host = Constants::SMTP_HOST;
        $this->mail->Port = Constants::SMTP_PORT;
        if (Constants::SMTP_TLS_REQUIRED) {
            $this->mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
        }
        $this->mail->SMTPAuth = Constants::SMTP_HAS_AUTH;
        if (Constants::SMTP_HAS_AUTH) {
            $this->mail->UserName = Constants::SMTP_USER;
            $this->mail->Password = Constants::SMTP_PASS;
        }
        $this->mail->setFrom(Constants::SMTP_FROM[0], Constants::SMTP_FROM[1]);
        $this->mail->addReplyTo(Constants::SMTP_FROM[0], Constants::SMTP_FROM[1]);
    }

    /*
     *
     * name: email
     *
     * Schedule an email that the cron based queue system can deliver asynchronously
     *
     * @param array $data key value pair that is passed to the template file to create email content.
     * @param string $template_name, the filename of the template file, without extension, that it can be taken from template directory and parsed.
     * @param string $subject subject for the email.
     * @access public
    */

    public function email($to = [], $data, $subject = Constants::EMAIL_DEFAULT_SUBJECT, $template = Constants::EMAIL_TEMPLATE_DEFAULT)
    {
        $params = [];
        if(is_string($data)) {
            $data = str_getcsv($data);
            foreach($data as $dat) {
                $key_val = explode(":", $dat);
                $params[$key_val[0]] = $key_val[1];
            }
        } else {
            $params = $data;
        }
        if(is_string($to)) {
            $to_data = explode(":", $to);
            $to = [];
            $to["name"] = $to_data[0];
            $to["email"] = $to_data[1];
        }
        $body = $this->parseTemplate($params, $template);
        $email_data = ["message" => [ "to" => $to, "subject" => $subject, "body" => $body ]];
        $this->schedule( $email_data );
        return ["success" => true ];
    }

    /*
     *
     * name: run
     *
     * that will be called from the Cron Shell class.
     * This will run asynchronously via cron shell and is fault tolerant to ensure email being sent.
     * This method is static that the thread can directly evoke without an instantiation
     *
     * @see \Com\Angeldrome\Shells\Crons::run()
     * @access public
     *
    */

    public function run()
    {
        try {
            $rows = $this->db->get(SQLs::SELECT_EMAIL_DATA);
            if (sizeof($rows) > 0) {
                foreach ($rows as $row) {
                    $data = [];
                    $data["attempts"] = $row["attempts"] + 1;
                    $data["last_error"] = "";
                    $mail_data = json_decode($row["message"], true);
                    if(is_array($mail_data) && isset($mail_data["message"])) {
                        $mail_data = $mail_data["message"];
                        $this->mail->addAddress($mail_data['to']['email'], $mail_data['to']['name']);
                        $this->mail->subject = $mail_data['subject'];
                        $this->mail->Body = $mail_data['body'];
                        $mail_resp = $this->mail->send();
                        if ($mail_resp) {
                            $data["delivered"] = 1;
                            $data["delivered_ts"] = date("Y-m-d H:i:s");
                        } else {
                            if ($data["attempts"] > Constants::EMAIL_MAX_ATTEMPTS) {
                                $data["aborted"] = 1;
                                $data["delivered"] = 0;
                                $data["last_error"] = "EMAIL SEND FAILED AND TOO MANY ATTEMPTED.";
                            }
                            //print $this->mail->ErrorInfo;
                            $data["last_error"] .= $this->mail->ErrorInfo;
                        }
                        $this->db->update(SQLs::EMAIL_TABLE, $data, ["id" => $row["id"]]);
                    }
                }
            }
        } catch (PHPMailer\PHPMailer\Exception $ex) {
            print($ex->getMessage);
            //TODO:: log the failure reason
            $data = [];
            $data["attempts"] = $row["attempts"] + 1;
            $data["delivered"] = 0;
            $data["last_error"] = $ex->getMessage();
            $this->db->update(Constants::EMAIL_TABLE, $data, ["id" => $row["id"]]);
        }
    }


    /*
     *
     * name: schedule
     *
     * a private method that stores serialized mail object, that we can send email asynchronously.
     *
     * @access private
    */

private function schedule($data)
    {
        $data["message"] = json_encode($data, true);
        $data["delivered_ts"] = null;
        $data["delivered"] = 0;
        $data["attempts"] = 0;
        $data["aborted"] = 0;
        $data["last_error"] = "";
        DBSave::run(Constants::EMAIL_DB, Constants::DB_OPERATION_INSERT, SQLs::EMAIL_TABLE, json_encode($data, true));
    }

    /*
     *
     * name: parseTemplate
     *
     * Read the given template file, that forms the body for email, and replace with content and form complete message.
     *
     * @param array $data key value pairs for replacing text in template to form the message.
     * @param string $template_name, the filename of the template file, without extension, that it can be taken from template directory and parsed.
     * @return string|null  returns parsed content as string or null in case, where there is no template file present in the template directory.
     * @access private
     */

    private function parseTemplate($data, $template_name = Constants::EMAIL_TEMPLATE_DEFAULT)
    {
        $tpl_file = Constants::getHomePath() . Constants::DS . "templates" . Constants::DS . "email" . Constants::DS . $template_name . ".tpl";
        if (file_exists($tpl_file)) {
            $content = file_get_contents($tpl_file);
            if (!empty($content)) {
                foreach ($data as $key => $value) {
                    if(!is_string($value)) {
                        $value = print_r($value, true);
                    }
                    $content = preg_replace("{{%" . $key . "%}}", $value, $content);
                }
                return trim($content);
            }
        }
        return null;
    }

    /*
     *
     * name: createSchema
     *
     * an one time running method to create the required table to enqueue emails for sending.
     *
     * @access private
    */
    private function createSchema()
    {
        $result = $this->db->get(SQLs::CHECK_EMAIL_TABLE);

        if ( sizeof($result) == 0 ) {
            $this->db->get(SQLs::CREATE_EMAIL_TABLE, [], false);
        } else {
            throw new DBException();
        }
    }
}
