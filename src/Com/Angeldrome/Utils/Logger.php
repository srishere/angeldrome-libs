<?php
/*
 * AngeldromeLibs, Some Base classes that could be reused for app development.
 *
 * Logger Util class, an sqlite based logger.
 *
 * @package    Com\Angeldrome\Utils
 * @author     Sriram R <srishere@angeldrome.com>
 * @copyright  2020 Sriram R
 * @license    This code is licensed under MIT license (see LICENSE.txt for details)
 * @version    CVS: $Id:$
 * @link       http://www.angeldrome.com
 */

namespace Com\Angeldrome\Utils;

use Com\Angeldrome\Libs\SQLite3;
use Com\Angeldrome\Configs\Constants;
use Com\Angeldrome\Configs\SQLs;
use Com\Angeldrome\Utils\General;
use Com\Angeldrome\Exceptions\DBException;
use Com\Angeldrome\Shells\DBSave;

class Logger
{
    private $db = null;

    /*
     *
     * name: __construct
     *
     * Default Constructor
     *
     * @param boolean $first_time to be used via an installer that the schema can be created, if this flag is set as true.
     *
     * @access public
     *
    */
    public function __construct($first_time = false)
    {

        if ($first_time) {
            $this->db = new SQLite3(Constants::LOGGER_DB);
            $this->createSchema();
        }
        if(file_exists(Constants::getDBDirectory().Constants::DS.Constants::LOGGER_DB)) {
            if(is_writeable(Constants::getDBDirectory().Constants::DS.Constants::LOGGER_DB)) {
                $this->db = new SQLite3(Constants::LOGGER_DB);
            } else {
                throw new DBException(Messages::LOGGER_DB_PERM_MSG, Messages::LOGGER_DB_PERM_CODE);
            }
        } else {
            throw new DBException(Messages::LOGGER_DB_MISSING_MSG, Messages::LOGGER_DB_MISSING_CODE);
        }
    }

    /*
     *
     * name: log
     *
     * method to record log
     *
     * @param int $id an asset id that realizes a user.
     * @param string $mode info, debug or error
     * @param mixed $msg the log message (can be an object also)
     * @access public
    */
    public function log($id, $mode, $msg)
    {
        $data = [];

        $data["asset_id"] = intval($id);
        $data["is_object"] =  0;

        if (!is_string($msg)) {
            $msg = json_encode($msg, true);
            $data["is_object"] =  1;
        }
        $data["severity"] = $mode;
        $data["message"] = $msg;
        $data["ip_address"] = General::getUserIP();
        $data["user_agent"] = General::getUserUA();
        DBSave::run(Constants::LOGGER_DB, Constants::DB_OPERATION_INSERT, SQLs::LOG_TABLE, json_encode($data));
        return ["success" => true, "data" => $data ];
    }

    /*
     *
     * name: getLogs
     *
     * get Log Data as csv that it can be used for rendering or store.
     *
     * @param string | null $dated for a particular date or for all dates.
     * @param string  $severity follow available constants to set the severity. if all is supplied, it will return all modes of logs.
     * @param string $pattern if the string has a valid input, will be included as like query pattern.
     * @return string  empty string if no results, or a csv data as string
     *
     * @see \Com\Angeldrome\Configs\Constants
     * @access public
    */
    public function getLogs($dated = null, $severity = Constants::LOGGER_SEVERITY_ALL, $pattern = "")
    {
        $conditions = "";
        $format = "Y-m-d";

        if ($severity != Constants::LOGGER_SEVERITY_ALL) {
            $conditions = " severity = '".$severity."' ";
        } else {
            $conditions = " severity IN ('".Constants::LOGGER_SEVERITY_INFO."', '".Constants::LOGGER_SEVERITY_DEBUG."', '".Constants::LOGGER_SEVERITY_ERROR."') ";
        }

        if ($dated != null) {
            $dated = date($format, strtotime($dated));
            $d = DateTime::createFromFormat($format, $dated);
            if ($d && $d->format($format) == $dated) {
                $conditions = " AND created = '".$dated."'";
            }
        }
        if (!empty(trim($pattern))) {
            $conditions = " AND log LIKE '%".$pattern."%' ";
        }
        $query = "SELECT * FROM ".SQLs::LOG_TABLE." WHERE ".$conditions." ORDER BY created DESC";
        $results = $this->db->get($query);
        if (is_array($results) && sizeof($results) > 0) {
            return json_encode($results);
        }
        return "";
    }

    /*
     *
     * name: createSchema
     *
     * an one time running method to create the required table to record logs.
     *
     * @access private
    */
    private function createSchema()
    {
        $result = $this->db->get(SQLs::CHECK_LOGGER_TABLE);
        if ( sizeof($result) == 0 ) {
            $query = SQLs::CREATE_LOGGER_TABLE;
            $this->db->get($query, [], false);
        } else {
            throw new DBException();
        }
    }
}
