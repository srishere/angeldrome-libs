<?php
/*
 * AngeldromeLibs, Some Base classes that could be reused for app development.
 *
 * General Utility class, methods that can be used in generic way
 *
 * @package    Com\Angeldrome\Utils
 * @author     Sriram R <srishere@angeldrome.com>
 * @copyright  2020 Sriram R
 * @license    This code is licensed under MIT license (see LICENSE.txt for details)
 * @version    CVS: $Id:$
 * @link       http://www.angeldrome.com
 */

    namespace Com\Angeldrome\Utils;

    class General
    {

        /*
         *
         * name: hasCurl
         *
         * Check if php curl extension exists.
         *
         * @return boolean  returns true or false based on the condition
         * @access public
         * @static
        */

        public static function hasCurl()
        {
            return (in_array('curl', get_loaded_extensions()));
        }

        /*
         *
         * name: getUserIP
         *
         * get remote user ip, parse and safe deliver
         *
         * @return string  ip address defaulting to IPV4 0.0.0.0
         *
         * @access public
         * @static
        */
        public static function getUserIP()
        {
            if(isset($_SERVER) && isset($_SERVER["REMOTE_ADDR"])) {
                return inet_pton($_SERVER["REMOTE_ADDR"]) ? $_SERVER["REMOTE_ADDR"] : "0.0.0.0";
            }
            return "0.0.0.0";
        }

        /*
         *
         * name: getUserUA
         *
         * get user agent, parse and safe deliver against xss
         *
         * @return string  user agent string
         *
         * @access public
         * @static
        */
        public static function getUserUA()
        {
            if(isset($_SERVER) && isset($_SERVER['HTTP_USER_AGENT'])) {
                return filter_var($_SERVER['HTTP_USER_AGENT'], FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            }
            return "NOT AVAILABLE";
        }
    }
