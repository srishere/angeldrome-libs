<?php
/**
 * AngeldromeLibs, Some Base classes that could be reused for app development.
 *
 * Curl base calls
 *
 * @author        Sriram
 * @link          https://www.angeldrome.com
 */

    namespace Com\Angeldrome\Libs;

    class Curl
    {
        private $curl = null;


        public function __construct()
        {
            $this->curl = curl_init();
        }

        public function post($call, $data = [])
        {
            curl_setopt($this->curl, CURLOPT_POST, 1);
            curl_setopt($this->curl, CURLOPT_POSTFIELDS, $data);
            curl_setopt($this->curl, CURLOPT_URL, $call);
            curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($this->curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            $resp = curl_exec($this->curl);
            curl_close($this->curl);
            $decoded = json_decode($resp);
            if (json_last_error() == JSON_ERROR_NONE) {
                return $decoded;
            } else {
                throw new CurlException("POST Invalid Response", 20001);
            }
            return false;
        }

        public function get($call)
        {
            curl_setopt($this->curl, CURLOPT_GET, 1);
            curl_setopt($this->curl, CURLOPT_URL, $call);
            curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
            $resp = curl_exec($this->curl);
            curl_close($this->curl);
            $decoded = json_decode($resp);
            if (json_last_error() == JSON_ERROR_NONE) {
                return $decoded;
            } else {
                throw new CurlException("POST Invalid Response", 20001);
            }
            return false;
        }

        private function setHeaders()
        {
        }
    }
