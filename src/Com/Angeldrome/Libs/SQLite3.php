<?php
/**
 * AngeldromeLibs, Some Base classes that could be reused for app development.
 *
 * A basic SQLite3 class
 *
 * @author        Sriram
 * @link          https://www.angeldrome.com
 */

    namespace Com\Angeldrome\Libs;

    use Com\Angeldrome\Configs\Messages;
    use Com\Angeldrome\Configs\Constants;
    use Com\Angeldrome\Exceptions\DBException;


    class SQLite3 extends \SQLite3
    {
        private $file_name = "";
        public function __construct($file_name)
        {
            $this->file_name = $file_name;
            try {
                $file_path = Constants::getDBDirectory().Constants::DS.$file_name;
                parent::__construct( $file_path, SQLITE3_OPEN_READWRITE | SQLITE3_OPEN_CREATE );
                $this->busyTimeout( intval(Constants::SQLITE3_TIMEOUT_SECONDS) * 1000 );
            } catch (Exception $ex) {
                //write to log
                throw new DBException(Messages::DB_INIT_FAIL_MSG, Messages::DB_INIT_FAIL_CODE);
            }
        }

        public function insert($table, $data)
        {
            $ts = date("Y-m-d H:i:s");
            try {
                $fields = implode("`, `", array_keys($data));
                $fields .= "`, `created`, `modified` ";
                $p_insert_stmt = "INSERT INTO ".$table." (`".$fields.") VALUES (";
                $ind = 0;
                foreach ($data as $key => $value) {
                    if ($ind > 0) {
                        $p_insert_stmt .= ", ";
                    }
                    $p_insert_stmt .= ":".$key;
                    ++$ind;
                }
                $p_insert_stmt .= ", '".$ts."','".$ts."')";
                $pstmt = $this->prepare($p_insert_stmt);
                if ($pstmt) {
                    foreach ($data as $key => $value) {
                        $pstmt->bindValue(":".$key, $value);
                    }
                    $result = $pstmt->execute();
                    if ($result) {
                        return $this->lastInsertRowID();
                    }
                    $this->writeToFile(Constants::DB_OPERATION_INSERT, $ts, $table, $data);
                    return false;
                } else {
                    $this->writeToFile(Constants::DB_OPERATION_INSERT, $ts, $table, $data);
                    throw new DBException(Messages::DB_INSERT_FAIL_MSG, Messages::DB_INSERT_FAIL_CODE);
                }
            } catch (Exception $ex) {
                $this->writeToFile(Constants::DB_OPERATION_INSERT, $ts, $table, $data);
                throw new DBException(Messages::DB_INSERT_FAIL_MSG, Messages::DB_INSERT_FAIL_CODE);
            }
        }

        public function update($table, $data = [], $conditions = [])
        {
            try {
                $ts = date("Y-m-d H:i:s");
                $fields = implode(", ", array_keys($data));
                $p_update_stmt = "UPDATE ".$table." SET modified = '".$ts."'";
                foreach ($data as $key => $value) {
                    $p_update_stmt .= ", `".$key."` = :".$key;
                }
                if (is_array($conditions) && sizeof($conditions) > 0) {
                    $p_update_stmt .= " WHERE ";
                    $ind = 0;
                    foreach ($conditions as  $key => $value) {
                        if ($ind > 0) {
                            $p_update_stmt .= ", ";
                        }
                        $p_update_stmt .= $key." = :".$key;
                        ++$ind;
                    }
                    $pstmt = $this->prepare($p_update_stmt);
                    if ($pstmt) {
                        foreach ($data as $key => $value) {
                            $pstmt->bindValue(":".$key, $value);
                        }
                        if (is_array($conditions) && sizeof($conditions) > 0) {
                            foreach ($conditions as $key => $value) {
                                $pstmt->bindValue(":".$key, $value);
                            }
                        }
                        if ( $pstmt->execute() ) {
                            return true;
                        }
                        $this->writeToFile(Constants::DB_OPERATION_UPDATE, $ts, $table, $data);
                        return false;
                    } else {
                        $this->writeToFile(Constants::DB_OPERATION_UPDATE, $ts, $table, $data);
                        throw new DBException(Messages::DB_UPDATE_FAIL_MSG, Messages::DB_UPDATE_FAIL_CODE);
                    }
                } else {
                    $this->writeToFile(Constants::DB_OPERATION_UPDATE, $ts, $table, $data);
                    throw new DBException(Messages::DB_UPDATE_FAIL_MSG, Messages::DB_UPDATE_FAIL_CODE);
                }
            } catch (Exception $ex) {
                $this->writeToFile(Constants::DB_OPERATION_UPDATE, $ts, $table, $data);
                throw new DBException(Messages::DB_UPDATE_FAIL_MSG, Messages::DB_UPDATE_FAIL_CODE);
            }
        }

        public function get($prepared = "", $data = [], $fetch = true)
        {
            try {
                $pstmt = $this->prepare($prepared);
                if ($pstmt) {
                    if (sizeof($data) == $pstmt->paramCount()) {
                        foreach ($data as $bind_attrs) {
                            $pstmt->bindValue($bind_attrs["field"], $bind_attrs["value"], $bind_attrs["data_type"]);
                        }
                        $result = $pstmt->execute();
                        if ($result && $fetch) {
                            $ret_array = [];
                            while($row = $result->fetchArray(SQLITE3_ASSOC)) {
                                $ret_array[] = $row;
                            }
                            return ($ret_array);
                        } else if( $result )  {
                            return $result;
                        }
                        return [];
                    } else {
                        throw new DBException(Messages::DB_SELECT_FAIL_MSG, Messages::DB_SELECT_FAIL_CODE);
                    }
                } else {
                    throw new DBException(Messages::DB_SELECT_FAIL_MSG, Messages::DB_SELECT_FAIL_CODE);
                }
            } catch (Exception $ex) {
                throw new DBException(Messages::DB_SELECT_FAIL_MSG, Messages::DB_SELECT_FAIL_CODE);
            }
            return [];
        }

        private function writeToFile($mode, $ts, $table, $data)
        {
            $file = Constants::getDBDirectory().Constants::DS."alt".Constants::DS.$this->file_name.".txt";
            $data = json_encode([ "method" => $mode, "ts" => $ts, "table" =>$table, "data" => $data ])."\n";
            file_put_contents($file, $data, FILE_APPEND | LOCK_EX);
        }
    }
