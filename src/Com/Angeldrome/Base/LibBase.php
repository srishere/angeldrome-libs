<?php
/*
 * AngeldromeLibs, Some Base classes that could be reused for app development.
 *
 * Base for all Lib classes
 *
 * @package    Com\Angeldrome\Base
 * @author     Sriram R <srishere@angeldrome.com>
 * @copyright  2020 Sriram R
 * @license    This code is licensed under MIT license (see LICENSE.txt for details)
 * @link       http://www.angeldrome.com
 */

namespace Com\Angeldrome\Base;

use Com\Angeldrome\Utils\General;

use Com\Angeldrome\Configs\Messages;
use Com\Angeldrome\Configs\Constants;
use Com\Angeldrome\Exceptions\GenericException;

abstract class LibBase
{
    private static $checked = false;
    private $logger = null;
    private $emailer = null;

    /*
     *
     * name: __construct
     *
     * Base Class Constructor,
     *
     * @throws GenericException  since this class try to get environment set right for one time run, it throws exception if some config or file goes missing.
     * @note Though db create call can create the files also, we do not want a sitation, where the file just gets created and app resume to work. So throwing exception,
     * to let know on the situation.
     *
     * @access public
     */

    public function __construct()
    {
        //after first time run, do set the constants value to false that unwanted processing is avoided.
        self::$checked = Constants::INSTALL_MODE;

        $this->emailer = new \Com\Angeldrome\Utils\EmailHandler(self::$checked);
        $this->logger = new \Com\Angeldrome\Utils\Logger(self::$checked);

        set_error_handler([$this, "catchErrors"]);

        if (!self::$checked) {
            if (!file_exists(Constants::getDBDirectory(). Constants::DS . Constants::LOGGER_DB)) {
                throw new GenericException(Messages::LOGGER_DB_MISSING_MSG, Messages::LOGGER_DB_MISSING_CODE);
            }
            if (!file_exists(Constants::getDBDirectory(). Constants::DS . Constants::EMAIL_DB)) {
                throw new GenericException(Messages::EMAIL_DB_MISSING_MSG, Messages::EMAIL_DB_MISSING_CODE);
            }
            if (!General::hasCurl()) {
                throw new GenericException(Messages::NO_CURL_MSG, Messages::NO_CURL_CODE);
            }
            if (!class_exists('SQLite3')) {
                throw new GenericException(Messages::NO_SQLITE3_MSG, Messages::NO_SQLITE3_CODE);
            }
        }

    }

    /*
     *
     * name: catchErrors
     *
     * Global catch for all errors that happen in api consumer and get the user to redirect to error page or return status false.
     *
     * @param [data type][variable] [descirption]
     * @return [data type]  [descirption]
     * @throws exceptionclass  [description]
     * @access public
     * @static
    */
    public function catchErrors($err_number, $err_message, $file_name, $line_number, $extra_vars)
    {
        $error_data = [
                    "error_number" => $err_number,
                    "error_message" => $err_message,
                    "file" => $file_name,
                    "line" => $line_number,
                    "data" => $extra_vars,
                    "sess_id" => session_id()
                    ];
        $id = (isset($_SESSION) && isset($_SESSION['AUTH_USER_ID'])) ? intval($_SESSION['AUTH_USER_ID']) : 0;
        $this->error($error_data, $id);
        if (Constants::INFO_TO["name"] != "NOT_USED" && Constants::INFO_TO["email"] != "NOT_USED") {
            $this->sendEmail(Constants::INFO_TO, $error_data, Messages::ERROR_EMAIL_SUBJECT, Constants::EMAIL_TEMPLATE_ERRORS);
        }
        exit(json_encode([ "success" => false ]));
    }

    /*
     *
     * name: info
     *
     * debugger calls that tags a log data as info and stores in db.
     *
     * @param string|array|object $data the data to store as info level log.
     * @access protected
    */
    protected function info($data, $id = 0)
    {
        $this->logger->log($id, Constants::LOGGER_SEVERITY_INFO, $data);
    }

    /*
     *
     * name: debug
     *
     * debugger calls that tags a log data as debug and stores in db.
     *
     * @param string|array|object $data the data to store as debug level log.
     * @access protected
    */
    protected function debug($data, $id = 0)
    {
        $this->logger->log($id, Constants::LOGGER_SEVERITY_DEBUG, $data);
    }

    /*
     *
     * name: error
     *
     * debugger calls that tags a log data as error and stores in db.
     *
     * @param string|array|object $data the data to store as error level log.
     * @access protected
    */
    protected function error($data, $id = 0)
    {
        $this->logger->log($id, Constants::LOGGER_SEVERITY_ERROR, $data);
    }

    /*
     *
     * name: sendEmail
     *
     * send an email
     *
     * @param array $to to name and email, in the form of [ "name"  => "xadasad", "email" => "asdasda@asdasda.con" ]
     * @param array $data data that files up the template
     * @param string $subject email subject text
     * @param string $template name of the template that need to be used for forming the email body
     *
     * @access protected
    */
    protected function sendEmail($to, $data, $subject = Constants::EMAIL_DEFAULT_SUBJECT, $template = Constants::EMAIL_TEMPLATE_DEFAULT)
    {
        $this->emailer->email($to, $data, $subject, $template);
    }

    /*
     *
     * name: redirect
     *
     * a common call for redirecting to another page.
     *
     * @param string $page the file, that forms the URI to get redirected to.
     *
     * @access protected
     *
    */
    protected function redirect($page)
    {
        header("Location: ".$page);
    }
}
