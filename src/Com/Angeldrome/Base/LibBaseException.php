<?php
/*
 * AngeldromeLibs, Some Base classes that could be reused for app development.
 *
 * Base Exception Class.
 *
 * @package    Com\Angeldrome\Base
 * @author     Sriram R <srishere@angeldrome.com>
 * @copyright  2020 Sriram R
 * @license    This code is licensed under MIT license (see LICENSE.txt for details)
 * @version    CVS: $Id:$
 * @link       http://www.angeldrome.com
 */

namespace Com\Angeldrome\Base;

use Com\Angeldrome\Configs\Messages;

abstract class LibBaseException extends \Exception
{
    protected $message = "";
    protected $code    = 20000;

    /*
     *
     * name: __construct
     *
     * Constructor for the exception classes
     *
     * @param string $message Exception Message
     * @param string $code Exception code, though an integer value, sent as string.
     *
     * @access public
     *
    */
    public function __construct($message = Messages::UNDEFINED_EXCEPTION_MSG, $code = Messages::UNDEFINED_EXCEPTION_CODE)
    {
        $this->message = $message;
        $this->code = $code;
        parent::__construct($message, $code);
    }

    /*
     *
     * name: __toString
     *
     * toString method to supply the exception as a string data.
     *
     * @return string  exception dump in form of presented string data.
     *
     * @access public
     *
    */
    public function __toString()
    {
        return  "Error Code: ".$this->code."\n".
                "Message: ".$this->message."\n".
                "Trace: ".$this->getTraceAsString();
    }
}
