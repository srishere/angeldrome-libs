<?php
/**
 * AngeldromeLibs, Some Base classes that could be reused for app development.
 *
 * DB Exception class
 *
 * @author        Sriram
 * @link          https://www.angeldrome.com
 */

    namespace Com\Angeldrome\Exceptions;

    use Com\Angeldrome\Base\LibBaseException;

    class DBException extends LibBaseException
    {
        protected $message = "";
        protected $code    = 0;

        public function __construct($message = "General DB Exception", $code = 25000)
        {
            $this->message = $message;
            $this->code = $code;
            parent::__construct($message, $code);
        }
    }
