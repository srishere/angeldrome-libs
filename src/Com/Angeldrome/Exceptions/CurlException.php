<?php
/**
 * AngeldromeLibs, Some Base classes that could be reused for app development.
 *
 * Curl Exception class
 *
 * @author        Sriram
 * @link          https://www.angeldrome.com
 */

    namespace Com\Angeldrome\Exceptions;

    use Com\Angeldrome\Base\LibBaseException;

    class CurlException extends LibBaseException
    {
        protected $message = "";
        protected $code    = 0;

        public function __construct($message = "General Curl Exception", $code = 20004)
        {
            $this->message = $message;
            $this->code = $code;
            parent::__construct($message, $code);
        }

        public static function raiseConnectionFailed()
        {
            return new self("Server connection failed", 20006);
        }
    }
