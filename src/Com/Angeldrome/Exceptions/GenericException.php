<?php
/**
 * AngeldromeLibs, Some Base classes that could be reused for app development.
 *
 * Generic Exception class
 *
 * @author        Sriram
 * @link          https://www.angeldrome.com
 */

    namespace Com\Angeldrome\Exceptions;

    use Com\Angeldrome\Base\LibBaseException;

    class GenericException extends LibBaseException
    {
        public function __construct($message = "General Exception", $code = 20001)
        {
            parent::__construct($message, $code);
        }
    }
