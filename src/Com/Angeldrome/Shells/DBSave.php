<?php
/**
 * AngeldromeLibs, Some Base classes that could be reused for app development.
 *
 * make db run async
 *
 * @author        Sriram
 * @link          https://www.angeldrome.com
 */

    namespace Com\Angeldrome\Shells;

    use Com\Angeldrome\Libs\SQLite3;
    use Com\Angeldrome\Configs\Constants;

    class DBSave
    {
        private static $db = null;

        public static function run($file, $method, $table, $data)
        {
            $data = json_decode($data, true);
            if(is_array($data)) {
                self::$db = new SQLite3($file);
                switch($method) {
                    case Constants::DB_OPERATION_INSERT :
                        self::$db->insert($table, $data);
                        break;
                    case Constants::DB_OPERATION_UPDATE :
                        self::$db->update($table, $data);
                        break;
                }
            }
        }
    }
