<?php
/**
 * AngeldromeLibs, Some Base classes that could be reused for app development.
 *
 * Cron class that runs via shell, to thread and poll on pending items that run in background
 *
 * @author        Sriram
 * @link          https://www.angeldrome.com
 */

    namespace Com\Angeldrome\Shells;
    require_once("autoload.php");
    use Com\Angeldrome\Configs\Constants;
    use Com\Angeldrome\Configs\Messages;
    use Com\Angeldrome\Utils\EmailHandler;
    use Com\Angeldrome\Configs\SQLs;
    use Com\Angeldrome\Libs\SQLite3;

    class Cron
    {
        public static function run($mode = null)
        {
            switch ($mode) {
                case Constants::RUN_EMAIL:
                    (new EMailHandler())->run();
                    break;
                default:
                    exit( Messages::NO_SUITABLE_CRON."\n" );
            }
            exit(0);
        }
    }

    if( sizeof($argv) == 2 ) {
        Cron::run($argv[1]);
    }
