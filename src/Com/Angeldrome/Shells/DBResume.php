<?php
/**
 * AngeldromeLibs, Some Base classes that could be reused for app development.
 *
 * make db run async
 *
 * @author        Sriram
 * @link          https://www.angeldrome.com
 */

    namespace Com\Angeldrome\Shells;

    use Com\Angeldrome\Libs\SQLite3;
    use Com\Angeldrome\Configs\Constants;
    use Com\Angeldrome\Base\APIBase;

    class DBResume extends APIBase
    {
        private static $db = null;

        public static function run()
        {
            $files = [
                    Constants::LOGGER_DB => Constants::getDBDirectory().Constants::DS."alt".Constants::DS.Constants::LOGGER_DB.".txt",
                    Constants::EMAIL_DB => Constants::getDBDirectory().Constants::DS."alt".Constants::DS.Constants::EMAIL_DB.".txt"
            ];
            foreach ($files as $db_file => $file) {
                $rows = self::getData($file);
                if (is_array($rows) && sizeof($rows) > 0) {
                    self::emptyFile($file);
                    foreach ($row as $row) {
                        $method = $row["method"];
                        $table = $row["table"];
                        $data = $row["data"];
                        $data = json_decode($data, true);
                        if (is_array($data)) {
                            self::$db = new SQLite3($db_file);
                            switch ($method) {
                                case Constants::DB_OPERATION_INSERT:
                                    self::$db->insert($table, $data);
                                    break;
                                case Constants::DB_OPERATION_UPDATE:
                                    self::$db->update($table, $data);
                                    break;
                            }
                        }
                    }
                }
            }
        }

        private static function getData($file)
        {
            $rows = [];
            $fileObj = fopen($file, "r");
            if ($inputFile) {
                while (($line = fgets($fileObj)) !== false) {
                    $rows[] = json_decode($line);
                }
            }
            fclose($fileObj);
            return $rows;
        }

        private static function emptyFile($file)
        {
            file_put_contents($file, "", LOCK_EX);
        }
    }
