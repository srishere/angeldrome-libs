<?php
/*
 * AngeldromeLibs, Some Base classes that could be reused for app development.
 *
 * Autoloader for classes and methods, that are evoked in this namespace.
 *
 * @author     Sriram R <srishere@angeldrome.com>
 * @copyright  2020 Sriram R
 * @license    This code is licensed under MIT license (see LICENSE.txt for details)
 * @version    CVS: $Id:$
 * @link       http://www.angeldrome.com
 */
    //let these guys be here, till some good approach is established
        session_start();
        date_default_timezone_set('UTC');
        //error_reporting(0);
    //end of core phpsetting


spl_autoload_register(function ($className) {
    $parts =  explode("\\", $className);
    if($parts[0] == "Com" && $parts[1] == "Angeldrome") {
        if (sizeof($parts) > 2) {
            $class = $parts[sizeof($parts) - 1];
            $path = "src/";
            for ($pind = 0; $pind < (sizeof($parts) - 1); $pind++) {
                $path .= $parts[$pind] . "/";
            }
            $class = $path.$class;
        } else {
            $class = $parts[0];
        }
        require $class.".php";
    }
});
